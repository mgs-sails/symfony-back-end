<?php


namespace App\Controller;


use App\Entity\Order;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class OrderController extends AbstractController
{
    public function index()
    {
        $orders = $this->getDoctrine()->getRepository(Order::class)->findAll();

        return $this->json(
            [
                'orders' => $orders
            ]
        );
    }


    public function store()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $order = new Order();
        $order->setCreatedAt(new \DateTime());
        $entityManager->persist($order);
        $entityManager->flush();
        return $this->json(
            [
                'order' => $order
            ]
        );
    }

}
